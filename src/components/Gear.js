import React from "react"
import { StaticQuery, graphql } from "gatsby"

const SiteSection = ({ data }) => (
<div>
<h2 className="major">{data.markdownRemark.frontmatter.title}</h2>
<span className="image main"><img src={data.markdownRemark.frontmatter.image} alt="" /></span>
    <div
          className="content"
          dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }}
        />
</div>
)

export default function Clients(props) {
  return (
    <StaticQuery
      query={graphql`
  {
    markdownRemark(frontmatter: {title: {eq: "Gear"}}) {
      frontmatter {
        title
        image
      }
      html
    }
  }
`}
      render={data => <SiteSection data={data} {...props} />}
    />
  )
}