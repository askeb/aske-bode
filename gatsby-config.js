module.exports = {
  siteMetadata: {
    title: "Aske Bode - Mix and Mastering engineer",
    author: "Aske Bode",
    description: "Aske Bode - online mastering service",
    keywords: "mastering music, music mastering, mix mastering, mastering tutorial, mastering danmark, mastering fyn, mastering class, mastering audio, mastering musik, musik mastering, hvordan uploader man til spotify, musikproduktion, lydmaskinen, mastering"
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'gatsby-starter-default',
        short_name: 'starter',
        start_url: '/',
        background_color: '#663399',
        theme_color: '#663399',
        display: 'minimal-ui',
        icon: 'src/images/logo.png', // This path is relative to the root of the site.
      },
    },
    {
        resolve: `gatsby-source-filesystem`,
        options: {
        path: `src/data`,
        name: `markdown-pages`,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: "UA-36914415-1",
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: false,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
        exclude: ["/adnin/**"],
        // Delays sending pageview hits on route update (in milliseconds)
        pageTransitionDelay: 0,
        // Enables Google Optimize using your container Id
        defer: false,
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-offline',
    'gatsby-plugin-netlify-cms',
    'gatsby-transformer-remark'
  ],
}
