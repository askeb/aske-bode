import React from 'react'
import { Link } from 'gatsby'

import Main from '../components/Main'
import Layout from '../components/layout'

const SecondPage = () => (
  <Layout>
        <Main>
    <h1>Hi from the second page</h1>
    <p>Welcome to page 2</p>
    <Link to="/">Go back to the homepage</Link>
            </Main>
  </Layout>
)

export default SecondPage
