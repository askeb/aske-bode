---
title: Prices
image: /images/uploads/pic03.jpg
---
### MIX PRICES

2500 DKK per song

### MASTERING PRICES

500 DKK per song

Stem mastering: 750 DKK per song

*All prices are including VAT*