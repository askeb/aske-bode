import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import '../assets/scss/main.scss'

const Layout = ({ children, location }) => {

  let content;

  if (location && location.pathname === '/' || '/free-testmaster/') {
    content = (
      <div>
        {children}
      </div>
    )
  } else {
    content = (
      <div id="wrapper" className="page">
        <div>
          {children}
        </div>
      </div>
    )
  }

  return (
    <StaticQuery
      query={graphql`
        query SiteTitleQuery {
          site {
            siteMetadata {
              title
            }
          }
        }
      `}
      render={data => (
        <>
          <Helmet
            title={data.site.siteMetadata.title}
            meta={[
              { name: 'description', content: 'Aske Bode - online mastering and mix engineer. Analog and digital hybrid mastering. Selected clients include Hymns From Nineveh, Kill J, Eclectic Moniker and more' },
              { name: 'keywords', content: 'mastering music, music mastering, mix mastering, mastering tutorial, mastering danmark, mastering fyn, mastering class, mastering audio, mastering musik, musik mastering, hvordan uploader man til spotify, musikproduktion, lydmaskinen, mastering' },
            ]}
          >
            <html lang="en" />
          </Helmet>
          {content}
        </>
      )}
    />
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
