---
title: Gear
image: /images/uploads/new-power-light.jpg
---
### MONITORING

PSI A21-M\
Audeze LCD-X\
Universal Audio Apollo 16 Mk2\
RME ADI-2 DAC FS\
AMS Neve 8816 Summing Amp\
AMS Neve 8804 Fader Pack

### EQUALIZATION

Chandler TG12345 Curve Bender\
D.A.V. electronics BG3

### DYNAMICS

Manley Stereo Variable MU Compressor\
Audioscape Buss Compressor\
Overstayer M-A-S 8101\
Valley People Dyna-mite\
2x DBX 520