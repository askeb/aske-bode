import React from "react";
import PropTypes from "prop-types";

const Header = (props) => (
  <header id="header" style={props.timeout ? { display: "none" } : {}}>
    <div className="content">
      <div className="inner">
        <h1>Aske Bode</h1>
        <h2>Audio Engineer</h2>
        <h3>Mix / Mastering / FOH</h3>
      </div>
    </div>
    <nav>
      <ul>
        <li>
          <a
            href="javascript:;"
            onClick={() => {
              ga('set', 'page', '/Clients.html');
              ga('send', 'pageview');
              props.onOpenArticle("intro");
            }}
          >
            Clients
          </a>
        </li>
        <li>
          <a
            href="javascript:;"
            onClick={() => {
              ga('set', 'page', '/Gear.html');
              ga('send', 'pageview');
              props.onOpenArticle("work");
            }}
          >
            Gear
          </a>
        </li>
        <li>
          <a
            href="javascript:;"
            onClick={() => {
              ga('set', 'page', '/Prices.html');
              ga('send', 'pageview');
              props.onOpenArticle("about");
            }}
          >
            Prices
          </a>
        </li>
        <li>
          <a
            href="javascript:;"
            onClick={() => {
              ga('set', 'page', '/Contact.html');
              ga('send', 'pageview');
              props.onOpenArticle("contact");
            }}
          >
            Contact
          </a>
        </li>
      </ul>
    </nav>
  </header>
);

Header.propTypes = {
  onOpenArticle: PropTypes.func,
  timeout: PropTypes.bool,
};

export default Header;