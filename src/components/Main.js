import React from "react";
import PropTypes from "prop-types";
import axios from "axios";
import * as emailValidator from "email-validator";
import { graphql } from "gatsby";
import Clients from "./Clients";
import Gear from "./Gear";
import Prices from "./Prices";

class Main extends React.Component {
  state = {
    name: "",
    email: "",
    message: "",
  };

  validateField = (field, isSubmit) => {
    const { name, email, message } = this.state;
    if (!isSubmit && this.state[field] === "") return;
    switch (field) {
      case "name":
        if (name !== "") {
          this.setState({ nameError: "" });
          return true;
        } else {
          this.setState({ nameError: "Enter your name" });
          return false;
        }
      case "email":
        if (email !== "") {
          if (!emailValidator.validate(email)) {
            this.setState({ emailError: "Enter a valid email address" });
            return false;
          }
          this.setState({ emailError: "" });
          return true;
        } else {
          this.setState({ emailError: "Enter your email address" });
          return false;
        }
      case "message":
        if (message !== "") {
          this.setState({ messageError: "" });
          return true;
        } else {
          this.setState({ messageError: "Enter a message" });
          return false;
        }
      default:
        break;
    }
  };

  validate = () => {
    const { name, email, message } = this.state;
    let isValid;
    this.validateField("name", true);
    this.validateField("email", true);
    this.validateField("message", true);
    isValid =
      this.validateField("name", true) &&
      this.validateField("email", true) &&
      this.validateField("message", true);
    if (isValid) {
      axios
        .post("https://formcarry.com/s/U5bZLv1D6Bm", {
          name,
          email,
          message,
        })
        .then((data) => console.log(data))
        .catch((error) => console.log(error));
    }
  };

  handleChange = (e) => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState(
      {
        [name]: value,
      },
      () => this.validateField(name, false)
    );
  };

  render() {
    let close = (
      <div
        className="close"
        onClick={() => {
          this.props.onCloseArticle();
        }}
      ></div>
    );

    return (
      <div
        ref={this.props.setWrapperRef}
        id="main"
        style={this.props.timeout ? { display: "flex" } : { display: "none" }}
      >
        <article
          id="intro"
          className={`${this.props.article === "intro" ? "active" : ""} ${
            this.props.articleTimeout ? "timeout" : ""
          }`}
          style={{ display: "none" }}
        >
          <Clients />
          {close}
        </article>

        <article
          id="work"
          className={`${this.props.article === "work" ? "active" : ""} ${
            this.props.articleTimeout ? "timeout" : ""
          }`}
          style={{ display: "none" }}
        >
          <Gear />
          {close}
        </article>

        <article
          id="about"
          className={`${this.props.article === "about" ? "active" : ""} ${
            this.props.articleTimeout ? "timeout" : ""
          }`}
          style={{ display: "none" }}
        >
          <Prices />
          {close}
        </article>

        <article
          id="contact"
          className={`${this.props.article === "contact" ? "active" : ""} ${
            this.props.articleTimeout ? "timeout" : ""
          }`}
          style={{ display: "none" }}
        >
          <h2 className="major">Contact</h2>
          <h3
            value={this.state.sent === "yes" ? "Your message was sent" : ""}
          />
          <p>Call Me on <a onClick="ga('send', 'event', 'Contact', 'Phone')" href="tel:+4522940935">+45 22 940 935</a><br>Write me at <a onClick="ga('send', 'event', 'Contact', 'Email')" href="mailto:mail@askebode.com">mail@askebode.com</a><br><a onClick="ga('send', 'event', 'Contact', 'Messenger')" href="http://m.me/askebode">Send me a message via Facebook</a><br>- or use the form below:</p>
          <form onClick="ga('send', 'event', 'Form', 'Sent')" method="post" action="https://getform.io/f/8e468199-c515-4a64-a261-3bed3b5a912a" accept-charset="UTF-8" >
            <div className="field half first">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                name="name"
                id="name"
                value={this.state.value}
                onBlur={this.handleChange}
              />
            </div>
            <div className="field half">
              <label htmlFor="email">Email</label>
              <input
                type="text"
                name="email"
                id="email"
                value={this.state.value}
                onBlur={this.handleChange}
              />
            </div>
            <div className="field">
              <label htmlFor="message">Message</label>
              <textarea
                name="message"
                id="message"
                value={this.state.value}
                onBlur={this.handleChange}
                rows="4"
              />
            </div>
            <ul className="actions">
              <li>
                <input type="submit" value="Send Message" className="special" />
              </li>
              <li>
                <input type="reset" value="Reset" />
              </li>
            </ul>
          </form>
          <ul className="icons">
            <li>
              <a
                href="http://twitter.com/pinligtavshed"
                className="icon fa-twitter"
              >
                <span className="label">Twitter</span>
              </a>
            </li>
            <li>
              <a
                href="http://facebook.com/askebode"
                className="icon fa-facebook"
              >
                <span className="label">Facebook</span>
              </a>
            </li>
            <li>
              <a
                href="http://instagram.com/askebode"
                className="icon fa-instagram"
              >
                <span className="label">Instagram</span>
              </a>
            </li>
          </ul>
          {close}
        </article>
      </div>
    );
  }
}

Main.propTypes = {
  route: PropTypes.object,
  article: PropTypes.string,
  articleTimeout: PropTypes.bool,
  onCloseArticle: PropTypes.func,
  timeout: PropTypes.bool,
  setWrapperRef: PropTypes.func.isRequired,
};

export default Main;
